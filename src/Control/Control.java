/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Modelo.Comentario;
import Modelo.Grupo;
import Modelo.Usuarios;
import Vista.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Rafael Sarmiento
 */
public class Control implements ActionListener {

    private ArrayList<Usuarios> lusuarios = new ArrayList<>();
    private ArrayList<Comentario> comentarios = new ArrayList<>();
    private ArrayList<Grupo> grupos = new ArrayList<>();

    String sesionLog;

    Inicio ini;
    Principal pri;
    Registro re;

    public Control(Inicio init, Principal prin, Registro regi) {
        super();
        this.ini = init;
        this.pri = prin;
        this.re = regi;
        actionListener(this);
    }

    private void actionListener(ActionListener evt) {

        ini.btnIniciarSesion.addActionListener(evt);
        ini.btnGoRegistrar.addActionListener(evt);

        re.btnRegistrar.addActionListener(evt);

        pri.btnActualizar.addActionListener(evt);
        pri.btnCrearComentario.addActionListener(evt);
        pri.btnMostrarComentarios.addActionListener(evt);
        pri.btnCrearGrupo.addActionListener(evt);
        pri.btnObtenerDatos.addActionListener(evt);
        pri.btnAsignarGrupo.addActionListener(evt);
        pri.btnCerrarSesion.addActionListener(evt);

    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        try {

            if (evt.getActionCommand().contentEquals("Iniciar Sesion")) {

                String user = ini.txtUser.getText();
                String pass = ini.txtPass.getText();

                if ("".equals(user) || "".equals(pass)) {
                    JOptionPane.showMessageDialog(null, "Alguno de los campos esta vacio");
                } else {
                    for (int i = 0; i < lusuarios.size(); i++) {
                        if (lusuarios.get(i).getCorreo().equals(user)
                                && lusuarios.get(i).getClaveAcceso().equals(pass)) {
                            sesionLog = user;
                            ini.setVisible(false);
                            pri.setVisible(true);

                            ini.txtUser.setText("");
                            ini.txtPass.setText("");
                        }
                    }
                }

            } else if (evt.getActionCommand().contentEquals("Registrarte")) {

                System.out.println("abriendo ventana de registrar");
                ini.setVisible(false);
                re.setVisible(true);

            } else if (evt.getActionCommand().contentEquals("Registrar")) {

                String nombre = re.txtNombre.getText();
                String apellido = re.txtApellido.getText();
                String nick = re.txtNick.getText();
                String edad = re.txtEdad.getText();
                String correo = re.txtCorreo.getText();
                String contraseña = re.txtContraseña.getText();

                System.out.println(nombre);
                System.out.println(apellido);
                System.out.println(nick);
                System.out.println(edad);
                System.out.println(correo);
                System.out.println(contraseña);

                if ("".equals(nombre) || "".equals(apellido) || "".equals(nick) || "".equals(edad)
                        || "".equals(correo) || "".equals(contraseña)) {
                    System.out.println("ESTO ESTA VACIO AAAAAA");
                    JOptionPane.showMessageDialog(null, "ALGUN CAMPO ESTA VACIO");
                } else if (isRegistrado(correo)) {
                    JOptionPane.showMessageDialog(null, "El correo ya se encuentra registrado con anterioridad");
                } else {
                    Usuarios user = new Usuarios(nombre, apellido, nick, edad, correo, contraseña);
                    System.out.println("" + user.toString());
                    lusuarios.add(user);
                    ini.setVisible(true);
                    re.setVisible(false);

                    re.txtNombre.setText("");
                    re.txtApellido.setText("");
                    re.txtNick.setText("");
                    re.txtEdad.setText("");
                    re.txtCorreo.setText("");
                    re.txtContraseña.setText("");

                    /*for (int i = 0; i < usuarios.size(); i++) {
                        System.out.println(""+usuarios.get(i).toString());
                    }*/
                }

            } else if (evt.getActionCommand().contentEquals("Actualizar")) {

                pri.jCmbUsuarios.removeAllItems();
                pri.jCmbUsuarios2.removeAllItems();
                pri.jCmbUsuarios3.removeAllItems();

                for (int i = 0; i < lusuarios.size(); i++) {
                    pri.jCmbUsuarios.addItem(lusuarios.get(i).getCorreo());
                    pri.jCmbUsuarios2.addItem(lusuarios.get(i).getCorreo());
                    pri.jCmbUsuarios3.addItem(lusuarios.get(i).getCorreo());
                }

            } else if (evt.getActionCommand().contentEquals("Crear Comentario")) {

                String alQueComenta = (String) pri.jCmbUsuarios.getSelectedItem();
                String comentario = pri.txtAreaCrearComentario.getText();
                String elQueComenta = sesionLog;
                Date fecha = new Date();

                if ("".equals(comentario) || "".equals(alQueComenta)) {
                    JOptionPane.showMessageDialog(null, "No puede insertar un comentario vacio");
                } else if (alQueComenta.equals(elQueComenta)) {
                    JOptionPane.showMessageDialog(null, "No puedes agregar un comentario a ti mismo");
                } else {
                    Comentario com = new Comentario(alQueComenta, comentario, elQueComenta, fecha);
                    System.out.println("" + com.toString());
                    comentarios.add(com);
                }

            }else if (evt.getActionCommand().contentEquals("Mostrar Comentarios")) {
                
                String usuarioSeleccionado = (String) pri.jCmbUsuarios2.getSelectedItem();
                String sumaComentarios = "";
                for (int i = 0; i < comentarios.size(); i++) {
                    if (comentarios.get(i).getPerfilAlQueComenta().equals(usuarioSeleccionado)) {
                        sumaComentarios += comentarios.get(i).toString();
                    }
                }
                
                pri.txtAreaComentarios.setText(sumaComentarios);
                
            }else if (evt.getActionCommand().contentEquals("Crear Grupo")) {
                
                String propietario = sesionLog;
                String nomEquipo = pri.txtNombreGrupo.getText();
                String descripcion = pri.txtDescripcion.getText();
                String tipoGrupo = pri.txtTipoGrupo.getText();
                String noticias = pri.txtNoticiasRecientes.getText();
                String contacto = pri.txtContacto.getText();
                
                Grupo gru = new Grupo(propietario,nomEquipo,descripcion,tipoGrupo,noticias,contacto);
                
                grupos.add(gru);
                
            }else if (evt.getActionCommand().contentEquals("Obtener Datos")) {
                
                pri.jCmbGrupo.removeAllItems();

                for (int i = 0; i < grupos.size(); i++) {
                    pri.jCmbGrupo.addItem(grupos.get(i).getNombreDelGrupo());     
                }
                
            }else if (evt.getActionCommand().contentEquals("Asignar Grupo")) {
                
                /*String userSeleccionado = (String) pri.jCmbUsuarios3.getSelectedItem();
                String grupoSeleccionado = (String) pri.jCmbGrupo.getSelectedItem();
                
                for (int i = 0; i < grupos.size(); i++) {
                    if (grupos.get(i).getUsuarioPropietario().equals(userSeleccionado)) {
                        JOptionPane.showMessageDialog(null, "El usuario seleccionado ya es propietario de este grupo");
                    }
                }
                
                Usuarios us = new Usuarios();
                us = buscarJugador(userSeleccionado);
                
                String propietario = sesionLog;
                String nomEquipo = pri.txtNombreGrupo.getText();
                String descripcion = pri.txtDescripcion.getText();
                String tipoGrupo = pri.txtTipoGrupo.getText();
                String noticias = pri.txtNoticiasRecientes.getText();
                String contacto = pri.txtContacto.getText();
                */     
            }else if (evt.getActionCommand().contentEquals("Cerrar Sesion")) {
                sesionLog = "";
                
                pri.txtAreaComentarios.setText("");
                pri.txtAreaCrearComentario.setText("");
                pri.txtContacto.setText("");
                pri.txtDescripcion.setText("");
                pri.txtNombreGrupo.setText("");
                pri.txtNoticiasRecientes.setText("");
                pri.txtTipoGrupo.setText("");
                
                pri.setVisible(false);
                ini.setVisible(true);
            }

        } catch (Exception er) {
            System.err.println(er.getMessage());
        }
    }

    public boolean isRegistrado(String usuario) {

        for (int i = 0; i < lusuarios.size(); i++) {
            if (lusuarios.get(i).getCorreo().equals(usuario)) {
                return true;
            }
        }
        return false;

    }
    
    public Usuarios buscarJugador(String usuario){
        Usuarios us =  new Usuarios();
        
        for (int i = 0; i < lusuarios.size(); i++) {
            if (lusuarios.get(i).getCorreo().equals(usuario)) {
                us.setNombre(lusuarios.get(i).getNombre());
                us.setApellido(lusuarios.get(i).getApellido());
                us.setNick(lusuarios.get(i).getNick());
                us.setEdad(lusuarios.get(i).getEdad());
                us.setCorreo(lusuarios.get(i).getCorreo());
                us.setClaveAcceso(lusuarios.get(i).getClaveAcceso());
            }
        }
        
       return us;
    }

}
