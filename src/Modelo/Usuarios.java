/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Rafael Sarmiento
 */
public class Usuarios {
    
    private String nombre;
    private String apellido;
    private String nick;
    private String edad;
    private String claveAcceso;
    private String correo;
    private static int maxCom= 0;
    private String comentario = " ";
    
    //private int cantcom = 0;

    public Usuarios() {
    }

    public Usuarios(String nombre, String apellido, String nick, String edad, String correo, String claveAcceso ) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.nick = nick;
        this.edad = edad;
        this.correo = correo;
        this.claveAcceso = claveAcceso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getClaveAcceso() {
        return claveAcceso;
    }

    public void setClaveAcceso(String claveAcceso) {
        this.claveAcceso = claveAcceso;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    public void comentar(String x){
        
        Usuarios.maxCom++;
        if (maxCom <= 10){
            this.comentario = "- "+comentario + x + "\n - ";
        }else{
            System.out.println("Ya a alcanzado el maximo de 10 comentarios.");
        }
        
        
        
    }
    
    
/*
    public static int getMaxCom() {
        return maxCom;
    }

    public static void setMaxCom(int maxCom) {
        Usuarios.maxCom = maxCom;
    }
    
    public int getCantcom() {
        return cantcom;
    }

    public void setCantcom(int cantcom) {
        this.cantcom = cantcom;
    }
*/

    public String getComentario() {
        return comentario;
    }

    @Override
    public String toString() {
        return "Usuarios{" + "nombre=" + nombre + ", apellido=" + apellido + ", nick=" + nick + ", edad=" + edad + ", correo=" + correo + ", claveAcceso=" + claveAcceso ;
    }
    

    
    
    
    
    
}
