/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author Rafael Sarmiento
 */
public class Grupo {
    private String usuarioPropietario;
    private String nombreDelGrupo;
    private String descripcion;
    private String tipoGrupo;
    private String noticiasRecientes;
    private String contacto;
    
    private ArrayList<Usuarios> integrantes;

    public Grupo() {
    }

    public Grupo(String usuarioPropietario, String nombreDelGrupo, String descripcion, String tipoGrupo, String noticiasRecientes, String contacto) {
        this.usuarioPropietario = usuarioPropietario;
        this.nombreDelGrupo = nombreDelGrupo;
        this.descripcion = descripcion;
        this.tipoGrupo = tipoGrupo;
        this.noticiasRecientes = noticiasRecientes;
        this.contacto = contacto;
    }

    public Grupo(String usuarioPropietario, String nombreDelGrupo, String descripcion, String tipoGrupo, String noticiasRecientes, String contacto, Usuarios usuario) {
        this.usuarioPropietario = usuarioPropietario;
        this.nombreDelGrupo = nombreDelGrupo;
        this.descripcion = descripcion;
        this.tipoGrupo = tipoGrupo;
        this.noticiasRecientes = noticiasRecientes;
        this.contacto = contacto;
        this.integrantes.add(usuario);
    }
    
    

    

    public String getUsuarioPropietario() {
        return usuarioPropietario;
    }

    public void setUsuarioPropietario(String usuarioPropietario) {
        this.usuarioPropietario = usuarioPropietario;
    }

    public String getNombreDelGrupo() {
        return nombreDelGrupo;
    }

    public void setNombreDelGrupo(String nombreDelGrupo) {
        this.nombreDelGrupo = nombreDelGrupo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipoGrupo() {
        return tipoGrupo;
    }

    public void setTipoGrupo(String tipoGrupo) {
        this.tipoGrupo = tipoGrupo;
    }

    public String getNoticiasRecientes() {
        return noticiasRecientes;
    }

    public void setNoticiasRecientes(String noticiasRecientes) {
        this.noticiasRecientes = noticiasRecientes;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public ArrayList<Usuarios> getIntegrantes() {
        return integrantes;
    }

    public void setIntegrantes(ArrayList<Usuarios> integrantes) {
        this.integrantes = integrantes;
    }
    
    

    
    
    
    
}
