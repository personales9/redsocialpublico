/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author Rafael Sarmiento
 */
public class Fotografia {
    private String nombre;
    private String descripcion;
    private int x;
    private int y;
    private ArrayList<Usuarios> etiquetados ;

    public Fotografia() {
        this.etiquetados = new ArrayList<>();
    }

    public Fotografia(String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.etiquetados = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public ArrayList<Usuarios> getEtiquetados() {
        return etiquetados;
    }

    public void setEtiquetados(ArrayList<Usuarios> etiquetados) {
        this.etiquetados = etiquetados;
    }
    
    public void etiquetar(Usuarios x, Usuarios y){
        if (this.etiquetados.size() <= 3){
            this.etiquetados.add(x);
            this.etiquetados.add(y);
        
        }else if(this.etiquetados.size() == 4){
            this.etiquetados.add(x);
        
        }
            
        
    }
    
    
    
            
    
}
