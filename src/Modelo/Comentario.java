/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.util.Date;

/**
 *
 * @author adria
 */
public class Comentario {
    
    private String perfilAlQueComenta;
    private String comentario;
    private String perfilQueComento;
    private Date fecha;

    public Comentario() {
    }

    public Comentario(String perfilAlQueComenta, String comentario, String perfilQueComento, Date fecha) {
        this.perfilAlQueComenta = perfilAlQueComenta;
        this.comentario = comentario;
        this.perfilQueComento = perfilQueComento;
        this.fecha = fecha;
    }

    public String getPerfilAlQueComenta() {
        return perfilAlQueComenta;
    }

    public void setPerfilAlQueComenta(String perfilAlQueComenta) {
        this.perfilAlQueComenta = perfilAlQueComenta;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getPerfilQueComento() {
        return perfilQueComento;
    }

    public void setPerfilQueComento(String perfilQueComento) {
        this.perfilQueComento = perfilQueComento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "Comentario{" + "perfilAlQueComenta=" + perfilAlQueComenta + ", comentario=" + comentario + ", perfilQueComento=" + perfilQueComento + ", fecha=" + fecha + "}\n";
    }

    
    
    
    
}
